import { Geolocation } from './geolocation';
import { CarSpace } from './car-space';

export class Estacionamento{

    constructor(public id : String,public name : String,public geolocation : Geolocation,public carSpaces : CarSpace[]){}

}