import { CarSpaceType } from './car-space-type';

export class CarSpace {

    constructor(public id : String,public position : String,public isBusy : Boolean,public type : CarSpaceType){}

}