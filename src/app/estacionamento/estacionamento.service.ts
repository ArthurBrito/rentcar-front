import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Estacionamento } from '../model/estacionamento';

@Injectable()
export class EstacionamentoService { 

    private baseUrl : string = "http://localhost:5000/api/Parking";

    constructor(private _http: HttpClient) { }

    getAll() {
      return this._http.get(this.baseUrl);
    }
  
    get(userId : number) {
      return this._http.get(`${this.baseUrl}/${userId}`);
    }
  
    create(user : Estacionamento) {
      return this._http.post(`${this.baseUrl}`,user)
    }
  
    update(userId : number, user : Estacionamento) {
      return this._http.put(`${this.baseUrl}/${userId}/update`, user);
    }
  
    delete(userId : number) {
      return this._http.delete(`${this.baseUrl}/${userId}/delete`);
    }

}