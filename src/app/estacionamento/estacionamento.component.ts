import { Component, OnInit } from '@angular/core';
import { Estacionamento } from '../model/estacionamento';
import { Router } from '@angular/router';
import { EstacionamentoService } from './estacionamento.service';

@Component({
  selector: 'app-estacionamento',
  templateUrl: './estacionamento.component.html',
  styleUrls: ['./estacionamento.component.css']
})
export class EstacionamentoComponent implements OnInit {

  estacionamentos : Estacionamento[]

  constructor(private service : EstacionamentoService, private router: Router) { }

  ngOnInit() {
    this.service.getAll().subscribe(
      (sucessResult : Estacionamento[]) =>{
        console.log(sucessResult)
        this.estacionamentos = sucessResult
      },
      (errorResult) => {
        console.log(errorResult)
      }
    )
  }

  visualizarVagas(event, estacionamento : Estacionamento){
    this.router.navigateByUrl(`/estacionamento/${estacionamento.id}/vagas`);
  }

}
